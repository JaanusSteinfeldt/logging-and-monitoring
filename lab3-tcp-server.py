#! /usr/bin/python
import socket,os
import logging
from rfc5424logging import Rfc5424SysLogHandler

syslogserver="192.168.180.158"

logger = logging.getLogger('Syslogger')
logger.setLevel(logging.INFO)

sh = Rfc5424SysLogHandler(address=(syslogserver, 514))

formatter = logging.Formatter('%(levelno)s - %(host)s - %(app_name)s - %(asctime)s - %(process)d - %(message)s')
sh.setFormatter(formatter)

logger.addHandler(sh)

logger.info('a message', extra={'app_name': 'lab3-tcp-server.py', 'host': 'lab04lnx'})

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('127.0.0.1', 12345))
sock.listen(5)

while True:
    connection,address = sock.accept()
    buf = connection.recv(1024)
    print (buf)
    connection.send(buf)
    connection.close()
exit()
