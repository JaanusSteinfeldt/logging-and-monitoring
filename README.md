## Logging specification - group10
---LINUX---

- no sensitive data allowed (except debug level)
- format:
  - syslog RFC5424
- destination:
  - syslog default
  - syslog(3) API preferred over syslog network delivery
- FACILITY: always 1 (user-level messages)
- SEVERITY: 10 to 50
  - debug level (10): on demand
  - info level (20): informational notices
  - warning level (30): warnings
  - error level (40): all relevant runtime errors (i.e. network connection issues)
  - critical level (50): critical level errors
- HOSTNAME: yes, FQDN or IP
- APP-NAME: yes, filename of script/app
- TIMESTAMP: yes
- PROCESS-ID: optional
- MSGID: optional, if used then msgid to be defined by author of script/app
- MSG:
  - actual event message

---WINDOWS---

## Logging specification (windows) - group10

- no sensitive data allowed (except debug level)
- format:
  - syslog RFC5424
- destination:
  - syslog default
  - syslog(3) API preferred over syslog network delivery
- FACILITY: always 1 (user-level messages)
- SEVERITY: 10 to 50
  - debug level (10): on demand
  - info level (20): informational notices
  - warning level (30): warnings
  - error level (40): all relevant runtime errors (i.e. network connection issues)
  - critical level (50): critical level errors
- TIMESTAMP: yes
- HOSTNAME: yes, FQDN or IP
- APP-NAME: yes, filename of script/app
- PROCESS-ID: optional
- MSGID: optional, if used then msgid to be defined by author of script/app
- MSG:
  - actual event message

